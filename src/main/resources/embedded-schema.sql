-- *** for H2 ***
-- CREATE TABLE memo (
--     id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
--     title VARCHAR(250) NOT NULL,
--     content TEXT NOT NULL
-- );

-- *** for Derby ***
CREATE SCHEMA SA;	-- absense of this statement may cause "schema 'SA' does not exist" error

CREATE TABLE memo (
    id INTEGER PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
    title VARCHAR(250) NOT NULL,
    content CLOB NOT NULL
);

CREATE INDEX titleOnMemo ON memo (title);
