package com.example.dayflower.practice.spring4.embeddeddb;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@Slf4j
public class Main {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		MemoDao memoDao = context.getBean(MemoDao.class);

		memoDao.addMemo("hello", "Hello, World!");

		MemoModel memo;

		memo = memoDao.getMemo(1);
		log.debug("getMemo(1): {}", memo);

		memo = memoDao.findMemoByTitle("%el%");
		log.debug("findMemoByTitle(\"%el%\"): {}", memo);
	}

}
