package com.example.dayflower.practice.spring4.embeddeddb;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Dao
public interface MemoDao {
	@Select("SELECT * FROM memo WHERE id = #{id}")
	public MemoModel getMemo(@Param("id") int id);

	// see XML
	public MemoModel findMemoByTitle(@Param("title") String title);

	@Insert("INSERT INTO memo (title, content) VALUES (#{title}, #{content})")
	public void addMemo(@Param("title") String title, @Param("content") String content);
}
