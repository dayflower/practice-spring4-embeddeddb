package com.example.dayflower.practice.spring4.embeddeddb;

import lombok.Data;

@Data
public class MemoModel {
	private int id;
	private String title;
	private String content;
}
