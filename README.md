## 目的

* Spring の embeddeddb サポートを使ってみる。
* myBatis についても利用方法を学習する。

## 苦労したところ

* Derby でふつうに embedded database を利用しようとすると「schema 'SA' does not exist」というエラーが出てしまった。
    * H2 とかだと問題なく動いた。
    * http://db.apache.org/derby/faq.html#schema_exist によると、「A schema is only created by CREATE SCHEMA or creating an object (table etc.) in that schema (this is implicit schema creation).」とのことだけど、普通に CREATE TABLE とかあるだけの初期化 SQL スクリプト (jdbc:script) 実行時にエラーになってしまう。
    * `CREATE SCHEMA sa` をスクリプトにいれたら問題なく動くようになった。すでにスキーマがあるときに実行しても問題ないっぽい。
    * しかし一度うまくいくようになったら `CREATE SCHEMA sa` をはずしても、うまく動くようになった。
    * Derby adapter の実装みたら embedded だとメモリに保存してあるはずっぽいけどなんでだろう。。。
* myBatis で LIKE 検索しようと思って SQL に `WHERE title LIKE '%#{title}%'` と書いたらうまくいかなかった。
    * `WHERE title LIKE #{title}` として `title` argument に `"%hoge%"` といれておく必要があった。
* WHERE 句を動的 SQL でビルディングしていくのはつらそうな予感がしている。
    * WHERE 句だけ外部の SQL ビルダの出力を埋め込みたいけど、無理だろうな。
* `<mybatis:scan>` で自動スキャン対象となるクラスを限定するために自作の `@Dao` アノテーション (ロジック的にはなにもしていない) を実装してみた。うまく動いているかわからないけど。
